import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store'

import * as TutorialActions from './../../stores/tutorial/tutorial.actions';

import { Tutorial } from './../../models/tutorial.model';
import { TutorialState } from './../../stores/tutorial/tutorial.state';


@Component({
  selector: 'app-read',
  templateUrl: './read.component.html',
  styleUrls: ['./read.component.scss']
})
export class ReadComponent implements OnInit {

  tutorials: Observable<Tutorial[]>;
  
  constructor(private _store: Store<TutorialState>) { 
  }

  ngOnInit() {
    this.tutorials = this._store.select('tutorial');
  }

  deteleTutorial(index: number) {
    this._store.dispatch(new TutorialActions.RemoveTutorial(index));
  }
}
