import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store'

import * as TutorialActions from './../../stores/tutorial/tutorial.actions';

import { Tutorial } from './../../models/tutorial.model';
import { TutorialState } from './../../stores/tutorial/tutorial.state';


@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {

  constructor(private _store: Store<TutorialState>) { }

  ngOnInit() {
  }
  
  addTutorial(name, url) {
    this._store.dispatch(new TutorialActions.AddTutorial({ name: name, url: url }));
  }

}
