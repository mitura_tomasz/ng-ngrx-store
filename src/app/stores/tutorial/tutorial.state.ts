import { Tutorial } from './../../models/tutorial.model';

export interface TutorialState {
  readonly tutorial: Tutorial[];
}